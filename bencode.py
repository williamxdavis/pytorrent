#!/usr/bin/env python3
from collections import OrderedDict
import sys
import hashlib

def listparse(code, index):
    thelist = []
    index += 1

    while True:
        if decode(code, index) == 'end':
            return thelist, index + 1
        else:
            item, index = decode(code, index)
            thelist.append(item)


def intparse(code, index):
    index += 1
    myint = int(code[index:code.find('e'.encode('utf-8'),index)])
    return myint, index + len(str(myint)) + 1


def stringparse(code, index):

    stringSize = int(code[index:code.find(':'.encode('utf-8'),index)])
    index += len(str(stringSize) + ':' )
    return code[index:index + stringSize], index + stringSize


def dictparse(code, index):

    mydict = OrderedDict()
    index += 1

    while True:

        bentype = decode(code, index)
        if bentype == 'end':
            return mydict, index + 1
        else:
            key, index = stringparse(code, index)
            mydict[key], index = decode(code, index)
             
             
def decode(code, index):

    types = {100:"dictionary", 108:"list", 105:"integer", 101:"end"}

    if code[index] in types:

        if types[code[index]] == "dictionary":
            benobj, index = dictparse(code, index)
        elif types[code[index]] == "list":
            benobj, index = listparse(code, index)
        elif types[code[index]] == "integer":
            benobj, index = intparse(code, index)
        elif types[code[index]] == "end":
            return 'end'

    elif str(code[index]).isdigit():
        benobj, index = stringparse(code, index)

    return benobj, index


def bencode(pyobj):
    benstring = str()
    if type(pyobj) is OrderedDict:
        for k,v in pyobj.items():
            benstring += 'd' + str(len(str(k))) + ":" + k.decode('utf-8') + bencode(v)
    elif type(pyobj) is list:
        for item in pyobj:
            benstring += 'l' + bencode(item) + 'e'
    elif type(pyobj) is bytes:
        stringed = str(pyobj)[2:][:-1]
        benstring += str(len(stringed)) + ":" + stringed
    elif type(pyobj) is int:
        benstring += 'i' + str(pyobj) + 'e'
    return benstring

def main():
    with open(sys.argv[1],'rb') as torrent_file:
        rawlines = torrent_file.readlines()

        total = bytes()

        for l in rawlines:
            total += l
            #TODO these do indeed end with \n

        output, _ = decode(total, 0)
        #print(output)
        print(bencode(output))

        print(hashlib.sha1(bencode(output[b'info']).encode('utf-8')).hexdigest())
        #TODO is this the correct infohash??

if __name__ == '__main__':
    main()
